var express = require('express');

var app = express();


app.get('*', function (req, res) { // http verb read request and response
    res.send("hello");
});

app.listen(3000, function (err) {
    if(err){
        console.log(err);
    }else{
        console.log("app is listening on port 3000");
    }
});

